import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  // TODO: change this implementation from static to dynamic
  state = {
    name: '',
    region: ''
  };
  validateInput = (id) => {
    alert(`Field ${id.toUpperCase()} is required`);
  };
  updateNameInput = (event) => {
    const nameValue = event.target.value;
    nameValue.length !== 0 ? this.setState({name : nameValue}) : this.validateInput(event.target.id);
  };
  updateRegionInput = (event) => {
    const regValue = event.target.value;
    regValue.length !== 0 ? this.setState({region : regValue}) : this.validateInput(event.target.id);

  };
  buildLocationRows(locations) {
    let newLocations = [];
    for(let prop in locations) {
      newLocations.push(locations[prop])
    }
    const location = newLocations.map((loc, idx) =>
    <tr key={idx}>
        <td>{loc.name}</td>
        <td>{loc.region}</td>
    </tr>
    );
    return (location)

  };


  // TODO: change the implementation of the add_location button to retrieve the name and region via form input elements
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to Elminda</h2>
        </div>
        <div className='add-new-item-container'>
          <div className='add-new-item-box'>
            <label htmlFor="name">Name (required):</label>
            <input type='text' id='name' name='name' placeholder='for example: China' onBlur={this.updateNameInput}/>
          </div>
          <div className='add-new-item-box'>
            <label htmlFor="name">Region (required):</label>
            <input type='text' id='region' name='region' placeholder='for example: Asia' onBlur={this.updateRegionInput}/>
          </div>

          <div className='add-new-item-box'>
            <button id="add_location"
              onClick={() => this.props.addLocation({ ...this.state})}
            >
              Add Name and Region
            </button>
          </div>
        </div>

      <table>
        <thead>
          <tr><th>Name</th><th>Region</th></tr>
        </thead>
        <tbody>
          { this.buildLocationRows(this.props.locations) }
        </tbody>
      </table>
      </div>
    );
  }
}

export default App;
